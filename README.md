#BeanCreator
程序很简单，很小，代码也很容易看懂.没有做什么词法语法分析之类的复杂工作，就是简单的解析.效果还不错!
最后我将他打包成了一个工具放在soft下面，你可以直接使用，它可以：

- 用mysql的脚本生成insert语句的下半句:

![输入图片说明](http://git.oschina.net/uploads/images/2015/0721/173620_82168e6e_128629.png "在这里输入图片标题")

- 用mysql的脚本生成java bean的主体代码:

![输入图片说明](http://git.oschina.net/uploads/images/2015/0721/173508_201f3e0b_128629.png "在这里输入图片标题")

- 用mysql的脚本生成insert的dao方法的参数字段:

![输入图片说明](http://git.oschina.net/uploads/images/2015/0721/173556_5223afb8_128629.png "在这里输入图片标题")