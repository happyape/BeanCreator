package com.creator.impl;

public class ParmTypeAndAnno {
	private String type;
	private String anno;

	public ParmTypeAndAnno(){}

	public ParmTypeAndAnno(String type, String anno){
		this.type = type;
		this.anno = anno;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getAnno() {
		return anno;
	}

	public void setAnno(String anno) {
		this.anno = anno;
	}
}