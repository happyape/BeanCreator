package com.creator.impl;

import java.util.Set;

public class MySqlInsertBeanContentAdapter extends BeanContentAdapter{

	@Override
	public String getBean() {
		String result = "";
		String questionMark = "";
		StringBuilder resultBuilder = new StringBuilder("(");
		StringBuilder questionMarkBuilder = new StringBuilder();
		Set<String> keys = super.nameAndTypeAndAnno.keySet();
		for (String attr : keys) {
			resultBuilder.append(attr).append(",");
		}
		result = resultBuilder.toString().subSequence(0, resultBuilder.length()-1) + ") values (";
		for (int i = 0; i < keys.size(); i++) {
			questionMarkBuilder.append("?,");
		}
		questionMark = questionMarkBuilder.toString().subSequence(0, questionMarkBuilder.length()-1) + ")";
		return result + questionMark;
	}

}