package com.creator.impl;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;

import com.creator.api.BeanContent;

public class BeanContentAdapter implements BeanContent{
	Map<String, ParmTypeAndAnno> nameAndTypeAndAnno = new LinkedHashMap<String, ParmTypeAndAnno>();

	public void setAttrTypeAndName(String type, String anno, String name) {
		nameAndTypeAndAnno.put(name, new ParmTypeAndAnno(type, anno));
	}

	public String getBean() {
		StringBuilder result = new StringBuilder();
		Set<String> keys = nameAndTypeAndAnno.keySet();
		for (String attr : keys) {
			result.append("private ").append(nameAndTypeAndAnno.get(attr).getType()).append(" ").append(attr).append(";")
			.append(" //").append(nameAndTypeAndAnno.get(attr).getAnno()).append("\r\n").toString();
		}
		return result.toString();
	}

	public void clean() {
		nameAndTypeAndAnno.clear();
	}

}