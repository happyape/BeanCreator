package com.creator.impl;

import com.creator.api.Source;

public class SourceAdapter implements Source{
	private String content;

	public SourceAdapter(){}

	public SourceAdapter(String content){
		this.content = content;
	}
	
	public void setContent(String content) {
		this.content = content;
	}

	public String getContent() {
		return content;
	}
}