package com.creator.impl;

import java.util.Map;
import java.util.Set;

import com.creator.api.BeanContent;
import com.creator.api.Source;
import com.creator.api.SourceParser;
import com.creator.util.MySqlParserUtil;

public class MySqlSourceParser implements SourceParser{

	public BeanContent doParse(Source source, BeanContent beanContent) {
		Map<String, ParmTypeAndAnno> columns = MySqlParserUtil.getTheNameAndType(source.getContent());
		Set<String> keys = columns.keySet();
		beanContent.clean();
		for (String key : keys) {
			beanContent.setAttrTypeAndName(MySqlParserUtil.typeMapper(columns.get(key).getType()), columns.get(key).getAnno(), MySqlParserUtil.getTheRightName(key));
		}
		return beanContent;
	}
}