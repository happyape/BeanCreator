package com.creator.impl;

import java.util.Set;

public class MySqlInsertBeanParamContentAdapter extends BeanContentAdapter{

	@Override
	public String getBean() {
		StringBuilder result = new StringBuilder();
		Set<String> keys = nameAndTypeAndAnno.keySet();
		for (String attr : keys) {
			result.append(nameAndTypeAndAnno.get(attr).getType()).append(" ").append(attr).append(", ").toString();
		}
		return result.toString();
	}

}