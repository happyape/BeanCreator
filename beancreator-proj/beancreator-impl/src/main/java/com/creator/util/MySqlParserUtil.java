package com.creator.util;

import java.util.LinkedHashMap;
import java.util.Map;

import com.creator.impl.ParmTypeAndAnno;

public class MySqlParserUtil {

	public static Map<String, ParmTypeAndAnno> getTheNameAndType(String content){
		Map<String, ParmTypeAndAnno> columns = new LinkedHashMap<String, ParmTypeAndAnno>();
		String[] rows = content.split(",");
		for (String row : rows) {
			String[] sqlContent = row.trim().split(" +");
			String name = sqlContent[0].substring(1,sqlContent[0].length()-1);
			String type = sqlContent[1];
			String anno = sqlContent[sqlContent.length-1];
			if (anno.contains("'") && anno.length()>1){
				anno = (String) anno.subSequence(1, anno.length()-1);
			}
			if (null == anno || anno.trim().length() <= 0 ){
				anno = "暂无注释";
			}
			ParmTypeAndAnno parmTypeAndAnno = new ParmTypeAndAnno(type, anno);
			columns.put(name, parmTypeAndAnno);
		}
		return columns;
	}

	public static String getTheRightName(String name){
		String[] underLineNameParams = name.split("_");
		StringBuilder newName = new StringBuilder();
		for (int i = 0; i < underLineNameParams.length; i++) {
			if(0==i){
				newName.append(changeHeadToUpperOrLowerCase(underLineNameParams[i], false));
			} else{
				newName.append(changeHeadToUpperOrLowerCase(underLineNameParams[i], true));
			}
		}
		return newName.toString();
	}

	public static String changeHeadToUpperOrLowerCase(String word, boolean upper){
		StringBuilder string = new StringBuilder();
		char[] words = word.toCharArray();
		for (int i = 0; i < words.length; i++) {
			if(0==i){
				if (upper) string.append(new Character(words[i]).toString().toUpperCase());
				else string.append(new Character(words[i]).toString().toLowerCase());
			} else string.append(words[i]);
		}
		return string.toString(); 
	}

	public static String typeMapper(String sqlType){
		String mysqlType = sqlType;
		int parenthesisIndex = mysqlType.indexOf("(");
		if (-1 < parenthesisIndex){
			mysqlType = (String) mysqlType.subSequence(0, parenthesisIndex);
		}
		if (mysqlType.contains("int")){
			return "int";
		} else if(mysqlType.contains("char") || mysqlType.contains("datetime")){
			return "String";
		} else if(mysqlType.contains("double")){
			return "double";
		} else if(mysqlType.contains("float")){
			return "float";
		} else return mysqlType;
	}
	public static void main(String[] args) {
		String a = "''";
		System.out.println(a.substring(1, a.length()-1));
	}
}