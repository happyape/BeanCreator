package com.creator.api;

public interface Source {
	void setContent(String content);
	String getContent();
}