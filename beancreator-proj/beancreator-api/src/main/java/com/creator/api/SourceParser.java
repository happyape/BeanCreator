package com.creator.api;


public interface SourceParser {
	public BeanContent doParse(Source source, BeanContent beanContent);
}