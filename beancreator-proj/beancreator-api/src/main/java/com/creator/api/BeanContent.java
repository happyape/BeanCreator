package com.creator.api;


public interface BeanContent {
	void setAttrTypeAndName(String type, String anno, String name);
	String getBean();
	void clean();
}