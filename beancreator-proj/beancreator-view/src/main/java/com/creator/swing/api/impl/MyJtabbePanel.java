package com.creator.swing.api.impl;

import java.util.Map;

import javax.swing.JPanel;
import javax.swing.JTabbedPane;

import com.creator.swing.api.JtabbePanelInterface;

public class MyJtabbePanel implements JtabbePanelInterface{

	public JTabbedPane createJtabblePanel(Map<String, JPanel> panels) {
		JTabbedPane myPane = new JTabbedPane();
		for (String title : panels.keySet()) {
			JPanel jPanel = panels.get(title);
			myPane.addTab(title, jPanel);
		}
		return myPane;
	}

}