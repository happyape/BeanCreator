package com.creator.swing.api;

import javax.swing.JPanel;

import com.creator.api.BeanContent;
import com.creator.api.SourceParser;

public interface PanelInterface {
	public JPanel createMyPanel(SourceParser parser, BeanContent beanContent);
}