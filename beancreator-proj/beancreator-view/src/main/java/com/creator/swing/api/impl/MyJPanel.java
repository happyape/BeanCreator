package com.creator.swing.api.impl;

import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.border.TitledBorder;

import com.creator.api.BeanContent;
import com.creator.api.SourceParser;
import com.creator.impl.SourceAdapter;
import com.creator.swing.api.PanelInterface;

public class MyJPanel implements PanelInterface{

	public JPanel createMyPanel(final SourceParser parser, final BeanContent beanContent) {
		JPanel myPanel = new JPanel();
		JPanel downPanel = new JPanel();
		JButton create = new JButton("create");
		JButton clean = new JButton("clean");
		final JTextArea mysqlArea = new JTextArea(22, 20);
		JScrollPane mysqlScroll = new JScrollPane(mysqlArea);
		mysqlScroll.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
		mysqlScroll.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_NEVER);
		final JTextArea codeArea = new JTextArea(22, 20);
		JScrollPane codeScroll = new JScrollPane(codeArea);
		codeScroll.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
		codeScroll.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_NEVER);
		myPanel.setLayout(new BorderLayout());
		myPanel.add(mysqlScroll, BorderLayout.WEST);
		myPanel.add(codeScroll, BorderLayout.EAST);
		myPanel.add(downPanel, BorderLayout.SOUTH);
		mysqlArea.setBorder(new TitledBorder("mysql"));
		mysqlArea.setLineWrap(true);
		codeArea.setBorder(new TitledBorder("code"));
		codeArea.setLineWrap(true);
		codeArea.setWrapStyleWord(true);
		downPanel.add(create);
		downPanel.add(clean);
		create.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent e) {
				String text = mysqlArea.getText();
				if ("".equals(text.trim())){
					JOptionPane.showMessageDialog(null, "mysql语句不能为空!", "警告",JOptionPane.ERROR_MESSAGE);
					return;
				}
				try{
					codeArea.setText(parser.doParse(new SourceAdapter(text), beanContent).getBean());
				} catch(Exception e1){
					JOptionPane.showMessageDialog(null, "解析出问题了-.-!", "对不起",JOptionPane.ERROR_MESSAGE);
					e1.printStackTrace();
				}
			}
		});
		clean.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent e) {
				mysqlArea.setText("");
				codeArea.setText("");
			}
		});
		return myPanel;
	}

}