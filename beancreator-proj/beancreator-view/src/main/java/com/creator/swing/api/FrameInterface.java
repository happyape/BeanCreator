package com.creator.swing.api;

import javax.swing.JFrame;
import javax.swing.JTabbedPane;

public interface FrameInterface {
	public JFrame createFrame(JTabbedPane jTabbedPane, String title);
}