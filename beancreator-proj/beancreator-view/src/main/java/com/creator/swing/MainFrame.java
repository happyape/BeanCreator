package com.creator.swing;

import java.util.HashMap;
import java.util.Map;

import javax.swing.JPanel;

import com.creator.api.SourceParser;
import com.creator.impl.BeanContentAdapter;
import com.creator.impl.MySqlInsertBeanContentAdapter;
import com.creator.impl.MySqlInsertBeanParamContentAdapter;
import com.creator.impl.MySqlSourceParser;
import com.creator.swing.api.impl.MyJPanel;
import com.creator.swing.api.impl.MyJframe;
import com.creator.swing.api.impl.MyJtabbePanel;

public class MainFrame{

	public static void init(){
		MyJframe jframe = new MyJframe();
		MyJtabbePanel jTabbedPane = new MyJtabbePanel();
		MyJPanel panels = new MyJPanel();
		SourceParser mysqlParser = new MySqlSourceParser();
		Map<String, JPanel> panelsTabble = new HashMap<String, JPanel>();
		panelsTabble.put("生成bean", panels.createMyPanel(mysqlParser, new BeanContentAdapter()));
		panelsTabble.put("insert语句", panels.createMyPanel(mysqlParser, new MySqlInsertBeanContentAdapter()));
		panelsTabble.put("insert方法参数", panels.createMyPanel(mysqlParser, new MySqlInsertBeanParamContentAdapter()));
		jframe.createFrame(jTabbedPane.createJtabblePanel(panelsTabble), "代码生成器");
	}

	public static void main(String[] args) {
		MainFrame.init();
	}
}