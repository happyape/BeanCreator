package com.creator.swing.util;

import java.awt.Dimension;
import java.awt.Toolkit;

import javax.swing.JFrame;

public class FrameUtil {

	/**
	 * 居中显示Jframe
	 * @param frame
	 */
	public static void centerView(JFrame frame){
		Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
		screenSize = Toolkit.getDefaultToolkit().getScreenSize();
		frame.setPreferredSize(new Dimension(512,450));           
		int frameWidth = frame.getPreferredSize().width;
		int frameHeight = frame.getPreferredSize().height;
		frame.setSize(frameWidth, frameHeight);
		frame.setLocation((screenSize.width - frameWidth) / 2,(screenSize.height - frameHeight) / 2);
	}
}