package com.creator.swing.api.impl;

import javax.swing.JFrame;
import javax.swing.JTabbedPane;

import com.creator.swing.api.FrameInterface;
import com.creator.swing.util.FrameUtil;

public class MyJframe implements FrameInterface{

	public JFrame createFrame(JTabbedPane jTabbedPane, String title) {
		JFrame frame = new JFrame();
		frame.setSize(500, 500);
		frame.setVisible(true);
		frame.setTitle(title);
		frame.add(jTabbedPane);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
//		frame.setResizable(false);
		FrameUtil.centerView(frame);
		return frame;
	}

}