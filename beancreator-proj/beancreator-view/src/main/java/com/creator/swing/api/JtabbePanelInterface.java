package com.creator.swing.api;

import java.util.Map;

import javax.swing.JPanel;
import javax.swing.JTabbedPane;

public interface JtabbePanelInterface {

	public JTabbedPane createJtabblePanel(Map<String, JPanel> panels);
}